export default {
  userSearches: [],
  currentSearch: {},
  currentSearchTerm: "",
  initialSearchComplete: false,
  searchEngines: [
    {
      id: 0,
      name: "Google",
      url: "https://google.com/search?igu=1&q=",
      avatar: "/statics/imgs/Google.svg"
    },
    /*     {
      id: 3,
      name: "Startpage",
      url: "https://www.startpage.com/do/search?q=",
      avatar: "/statics/imgs/Startpage.svg"
    }, */
    {
      id: 3,
      name: "Baidu",
      url: "https://baidu.com/s?wd=",
      avatar: "/statics/imgs/Baidu.svg"
    },
    {
      id: 2,
      name: "Bing",
      url: "https://bing.com/search?q=",
      avatar: "/statics/imgs/Bing.svg"
    },
    /*    {
      id: 1,
      name: "DuckDuckGo",
      url: "https://duckduckgo.com/html/?q=",
      avatar: "/statics/imgs/DuckDuckGo.svg"
    },
*/
    // {
    //   id: 4,
    //   name: "Twitter",
    //   url: "https://twitter.com/search?q=",
    //   avatar: "/statics/imgs/Twitter.svg"
    // },
    {
      id: 5,
      name: "CreativeCommons",
      url: "https://search.creativecommons.org/search?q=",
      avatar: "/statics/imgs/CC.svg"
    },
    {
      id: 6,
      name: "Internet Archive",
      url: "https://archive.org/search.php?sin=TXT&query=",
      avatar: "/statics/imgs/InternetArchives.svg"
    },

    // {
    //   id: 7,
    //   name: "Yahoo!",
    //   url: "https://search.yahoo.com/search;_ylt=A0oG7l7PeB5P3G0AKASl87UF?p=",
    //   avatar: "/statics/imgs/search.svg"
    // },
    // {
    //   id: 8,
    //   name: "Yandex",
    //   url: "https://yandex.ru/search/?lr=102586&text=",
    //   avatar: "/statics/imgs/search.svg"
    // },
    // {
    //   id: 9,
    //   name: "Swisscows",
    //   url: "https://swisscows.com/web?query=",
    //   avatar: "/statics/imgs/search.svg"
    // },
    {
      id: 10,
      name: "Ecosia",
      url: "https://www.ecosia.org/search?q=",
      avatar: "/statics/imgs/Ecosia.svg"
    },
    {
      id: 11,
      name: "Wolfram Alpha",
      url: "https://www.wolframalpha.com/input/?i=",
      avatar: "/statics/imgs/Wolfram.svg"
    },
    // {
    //   id: 12,
    //   name: "Ask.com",
    //   url: "https://www.ask.com/web?o=&l=&qo=serpSearchTopBox&q=",
    //   avatar: "/statics/imgs/search.svg"
    // },
    // {
    //   id: 13,
    //   name: "AOL",
    //   url: "https://search.aol.com/aol/search?s_chn=prt_bon&s_it=comsearch&q=",
    //   avatar: "/statics/imgs/search.svg"
    // },
    // {
    //   id: 14,
    //   name: "Wow",
    //   url:
    //     "https://www.wow.com/search;_ylt=Awr9DtpBRmJdGrMAGI.oCmVH;_ylc=X1MDMTE5NzgwNDIwMARfcgMyBGZyAwRncHJpZANVNGt4MVoub1FaR04xeGVXX0Q3UVlBBG5fcnNsdAMwBG5fc3VnZwMxMARvcmlnaW4Dd3d3Lndvdy5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMEcXN0cmwDNgRxdWVyeQNob3JzZXMEdF9zdG1wAzE1NjY3MjE3NDY-?fr2=sb-top-&s_it=sb-home&iscqry=&q=",
    //   avatar: "/statics/imgs/search.svg"
    // },
    // {
    //   id: 15,
    //   name: "Stack Overflow",
    //   url: "https://stackoverflow.com/search?q=",
    //   avatar: "/statics/imgs/Stacko.svg"
    // },
    {
      id: 16,
      name: "dogpile",
      url: "https://www.dogpile.com/serp?q=",
      avatar: "/statics/imgs/Dogpile.svg"
    },
    {
      id: 17,
      name: "info.com",
      url: "https://www.info.com/serp?q=",
      avatar: "/statics/imgs/Info.svg"
    },
    {
      id: 18,
      name: "Sogou",
      url: "https://www.sogou.com/web?query=",
      avatar: "/statics/imgs/Sogou.svg"
    },
    {
      id: 19,
      name: "SearX",
      url: "https://sarchy.tech/?q=",
      avatar: "/statics/imgs/searx.svg"
    }
  ]
};
