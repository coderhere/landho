export function search(state) {
  return state.currentSearch;
}

export function userSearchHistory(state) {
  return state.userSearches;
}
export function searchEngines(state) {
  return state.searchEngines;
}

export function formattedHistory(state) {
  return state.userSearches.map(history => ({
    timestamp: history.timestamp,
    src: state.searchEngines.find(se => se.id === history.src),
    searchTerm: history.searchTerm
  }));
}

export function initialSearchComplete(state) {
  return state.initialSearchComplete;
}

export function currentSearchTerm(state) {
  return state.currentSearchTerm;
}
