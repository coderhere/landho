# Land Ho!

Land Ho allows users to perform web queries across popular internet search engines and save the history of their queries securely within their Blockstack account. Queries can then be repeated on different engines by simply toggling between them. And even though search history data can be accessed by the user (and the user alone), it can also be easily deleted at the user's discretion.

Our aim is to displace centralized systems of control by building bridges between familiar systems and new ones. Land Ho is only one piece of that puzzle, and we are proud to add one more way that users can slowly, but surely, take back control of their digital lives.

Challenges we’re aware of… despite the fact we’re quite happy with what we accomplished here in a short period of time. We had to rely on iframes which don't give much flexibility (to say the least) and some results won't load as a result without right-clicking to open the link in a new tab.

We hope to add new functionality, stability, and improved UX in the future. But in the mean time, we’re super excited to have you test out this first release with us!

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev -m pwa
```

### Lint the files

```bash
yarn run lint
```

### Build the app for production

```bash
quasar build -m pwa
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


### Donate

BTC: 3MhTdrjfnSYQWEHn8UstZMx9nsNpentkqF
